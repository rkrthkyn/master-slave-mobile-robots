function [s,reachedTopOfFile] = tail(filename,numLines)
%Read the last nLines of text from a text file by reading the file
%backwards from the end. Good for just getting the last small bit of data
%off the end of a large text file.
%
%
%Mike Thiem, Aug 29 2017
%mike.j.thiem@gmail.com

%% Setup
fid = fopen(filename,'r');
newlineChar = char(10);
try
    reachedTopOfFile = false;

    offset = -2;
    fseek(fid,-1,'eof');

    %Initialize
    s = '';
    newChar = fread(fid,1,'*char');
    numLinesRead = 0;

    while numLinesRead < numLines && ~reachedTopOfFile
        s = [newChar s];

        reachedTopOfFile = ftell(fid) == 1;
        fseek(fid,offset,'cof');

        newChar = fread(fid,1,'*char');

        if strcmp(newChar,newlineChar)
            numLinesRead = numLinesRead + 1;
        end
        
        
    end

catch ME
    %Be sure to close the file regardless of errors
    fclose(fid);
    rethrow(ME);
end

fclose(fid);
