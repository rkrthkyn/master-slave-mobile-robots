const int trigPin1 = 22;
const int echoPin1 = 23;
const int trigPin2 = 24;
const int echoPin2 = 25;
const int trigPin3 = 26;
const int echoPin3 = 27;

int u1, u2, u3;

void setup() {
pinMode(trigPin1, OUTPUT); 
pinMode(echoPin1, INPUT); 
pinMode(trigPin2, OUTPUT); 
pinMode(echoPin2, INPUT); 
pinMode(trigPin3, OUTPUT); 
pinMode(echoPin3, INPUT); 

Serial.begin(9600); 
}

void loop() {
u2 = Ultrasound_2();
u1 = Ultrasound_1();
delay(15);
u3 = Ultrasound_3(); 

//check if next to a boundary
if(u1<15){

// check the forward sensor value
if (u2<10){ 
  turnR(); // turn 90 deg clockwise
}

else{
  drive(); // drives the robot forward
}
}
else{
  turnL();
}

//recors sensor values
u2 = Ultrasound_2();
u1 = Ultrasound_1();
delay(15);
u3 = Ultrasound_3(); 

Transmit(); //transmits date to PC for mapping


}


int Ultrasound_1() {
long duration;
int distance;
 
digitalWrite(trigPin1, LOW);
delayMicroseconds(2);

// Sets the trigPin on HIGH state for 10 micro seconds
digitalWrite(trigPin1, HIGH);
delayMicroseconds(10);
digitalWrite(trigPin1, LOW);

// Reads the echoPin, returns the sound wave travel time in microseconds
duration = pulseIn(echoPin1, HIGH);

// Calculating the distance
distance= duration*0.034/2;

// Returns distance
return (distance);
}

int Ultrasound_2() {
long duration;
int distance;
 
digitalWrite(trigPin2, LOW);
delayMicroseconds(2);

digitalWrite(trigPin2, HIGH);
delayMicroseconds(10);
digitalWrite(trigPin2, LOW);

duration = pulseIn(echoPin2, HIGH);


distance= duration*0.034/2;

return (distance);
}

int Ultrasound_3() {
long duration;
int distance;
 
digitalWrite(trigPin3, LOW);
delayMicroseconds(2);

digitalWrite(trigPin3, HIGH);
delayMicroseconds(10);
digitalWrite(trigPin3, LOW);

duration = pulseIn(echoPin3, HIGH);


distance= duration*0.034/2;

return (distance);
}



