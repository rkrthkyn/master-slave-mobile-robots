/*MEEN 667 Mechatronics - Course Project
Arvind Srinivasa
Rohith Karthikeyan
Taimoor Daud Khan
Edit: 11/15/2017 Wednesday*/

// --------------------------------Headers/ Setup begin------------------------------------------------
	#include <AFMotor.h>             		 	//Include Motor Library
	#include <SoftwareSerial.h>					//Include SoftwareSerial Library
	#include <math.h>                  			//Include Math library
	AF_DCMotor motorRB(1,MOTOR12_64KHZ); 		//setup Right side rear motor
	AF_DCMotor motorLB(2,MOTOR12_64KHZ); 		//setup Left side rear motor
	AF_DCMotor motorRF(4,MOTOR34_64KHZ); 		//setup Right front motor
	AF_DCMotor motorLF(3,MOTOR34_64KHZ); 		//setup Left front motor
	#define pin_r 21         					// Encoder Pins Define
	#define pin_l 20 
// --------------------------------Headers/ Setup End------------------------------------------------
	
// --------------------------------Global Var begin ------------------------------------------------

//======================
//Sensor Pin Definitions
//======================

const int trigPin1 = 53;
const int echoPin1 = 52;
const int trigPin2 = 49;
const int echoPin2 = 48;
const int trigPin3 = 51;
const int echoPin3 = 50;

const int s0 = 39;  
const int s1 = 40;  
const int s2 = 42;  
const int s3 = 41;  
const int out = 43; 

const int buzzerPin = 25; 
const int groundPin = 31 ;
  
int blue = 0;  
int target = 0;
int red = 0;

//=======================
//MotoControl Definitions
//=======================

double encConst = 0.1859882;   //Encoder constant calibrated to move the bot by 20cm steps
int   pulseR = 0;
double botDist = 0;         //total distance travelled by the robot in mm
double diffBotDist = 0;
int dutyCycle; 
int startDuty = 100;          //Duty cycle to start the motors
int startDuty2 = 240;           //Duty cycle to start the motors
double kp = 1;                //Proportional Gain
double kd = 7;                //Derivative Gain
double ki = 100;        //Intergral Gain
int maxoutput = 70;     //Maximum duty cycle
double deadband = 10;         //Error Margin
double dt = 0.008;            //Encoder Update Rate
double error;
double derivative, integral;
double errorlast = 0;
double pi = 3.14159265359;
double Reference;
int botOrient = 0;          // 1,2,3,4 values tell me orientation of bot
double Theta = 0;
double SinNow = 0;
double CosNow = 1;
int botPosX = 0;
int botPosY = 0;
double diffBotPosX = 0;     //Dispacement along X during the function
double diffBotPosY = 0;     //Dispacement along Y during the function
int turnRightCounter = 0;

bool boolStop = false;
int loopCounter = 0;
//=======================
//Xbee Definitions
//=======================

SoftwareSerial XBee(2, 15); 		// RX, TX

//=======================
//Sensor Definitions
//=======================

struct sensorPing{
	int US1;
	int US2;
	int US3;
	String readColor;
	
};
sensorPing sensRd;
//======================
//Setup/Loop Functions
//======================

void setup() {
Serial.begin(9600); 			//standard baud rate
XBee.begin(9600);				// baud rate for Xbee object

/*For  Encoder Modules */
pinMode(pin_r,INPUT);
pinMode(pin_l,INPUT);

/*For Sensor Comm Modules */
pinMode(trigPin1, OUTPUT); 
pinMode(echoPin1, INPUT); 
pinMode(trigPin2, OUTPUT); 
pinMode(echoPin2, INPUT); 
pinMode(trigPin3, OUTPUT); 
pinMode(echoPin3, INPUT); 

/*ISR Definition for Encoders*/
attachInterrupt(digitalPinToInterrupt(pin_r),doEncoderR,CHANGE);

/*Pin Setup for Color Sensor*/
pinMode(s0, OUTPUT);  
pinMode(s1, OUTPUT);  
pinMode(s2, OUTPUT);  
pinMode(s3, OUTPUT);  
pinMode(out, INPUT);  
digitalWrite(s0, HIGH);  
digitalWrite(s1, HIGH);
digitalWrite(groundPin, LOW);

/*Pin Setup for Buzzer*/
pinMode(buzzerPin, OUTPUT); 
/*Pin Setup for Buzzer*/
//pinMode(switchPin, INPUT);
delay(5000);


}
void loop() {

while(!boolStop){
sensRd = ping();
Serial.println(sensRd.US2);
transmit();
delay(20000);
if (sensRd.US2 < 30){
		turnRight();
	}
	else{
		moveForward(20); // What value goes in here? - now it is an arbitrary 20 units
	}

	
	}
if(loopCounter<=2)
{
digitalWrite(buzzerPin,HIGH);
delay(200);
digitalWrite(buzzerPin,LOW);
}

loopCounter++;

}

// --------------------------------Addon Functions Begin ------------------------------------------------

//======================
//Sensor Calls
//======================
int Ultrasound_1() {
long duration;
int distance;
 
digitalWrite(trigPin1, LOW);
delayMicroseconds(2);

// Sets the trigPin on HIGH state for 10 micro seconds
// Sets the trigPin on HIGH state for 10 micro seconds
digitalWrite(trigPin1, HIGH);
delayMicroseconds(10);
digitalWrite(trigPin1, LOW);

// Reads the echoPin, returns the sound wave travel time in microseconds
duration = pulseIn(echoPin1, HIGH);

// Calculating the distance
distance= duration*0.034/2;

// Returns distance
return (distance);
}
int Ultrasound_2() {
long duration;
int distance;
 
digitalWrite(trigPin2, LOW);
delayMicroseconds(2);

digitalWrite(trigPin2, HIGH);
delayMicroseconds(10);
digitalWrite(trigPin2, LOW);

duration = pulseIn(echoPin2, HIGH);


distance= duration*0.034/2;

return (distance);
}
int Ultrasound_3() {
long duration;
int distance;
 
digitalWrite(trigPin3, LOW);
delayMicroseconds(2);

digitalWrite(trigPin3, HIGH);
delayMicroseconds(10);
digitalWrite(trigPin3, LOW);

duration = pulseIn(echoPin3, HIGH);


distance= duration*0.034/2;

return (distance);
}

int colorSensor()  
{    
  target=0;
  digitalWrite(s2, LOW);  
  digitalWrite(s3, LOW);
  red = pulseIn(out, digitalRead(out) == HIGH ? LOW : HIGH);
  digitalWrite(s3, HIGH);  
  blue = pulseIn(out, digitalRead(out) == HIGH ? LOW : HIGH);  
  if(blue<10 && blue>5)
  {
    target=1;
  }
  
 if(red<12 && red>11)
  {boolStop = true;}
     
  return target;
}

sensorPing ping(){
	
	sensorPing x;
	x.US1 = Ultrasound_1();
	delay(15);
	x.US2 = Ultrasound_2();
	delay(15);
	x.US3 = Ultrasound_3();
	x.readColor = colorSensor();

	return x;
}
//======================
//Motor Drive
//======================
void PID(){
  
  errorlast = error;
  error = Reference - botDist;


  if (abs(error) <= deadband)
  {
    dutyCycle = 0;
    error = 0;
  }
  else {
    dutyCycle = (kp*error) + (kd*derivative) + (ki*integral);
  }

  if (dutyCycle >= maxoutput)
    dutyCycle = maxoutput;
  else if (dutyCycle <= -maxoutput)
    dutyCycle = -maxoutput;
  else

    integral += error*dt;

  derivative = (error - errorlast) / dt;

}
void doEncoderR(){
  pulseR = pulseR + 1;
}

void updatedist(){
  botDist = encConst * pulseR;
  diffBotDist = encConst * pulseR; 
  error = Reference - botDist;
}

void moveForward( int posInput){
  Reference = posInput;
  motorRB.setSpeed(startDuty);       //run motor at dutycycle defined by PID
  motorRB.run(FORWARD);
  motorLB.setSpeed(startDuty); 
  motorLB.run(FORWARD);
  motorRF.setSpeed(startDuty);
  motorRF.run(FORWARD);
  motorLF.setSpeed(startDuty);
  motorLF.run(FORWARD);
  delay(100);
   do
  { 
  updatedist();
  PID();
  motorRB.setSpeed(dutyCycle);     //run motor at dutycycle defined by PID
  motorRB.run(FORWARD);
  motorLB.setSpeed(dutyCycle); 
  motorLB.run(FORWARD);
  motorRF.setSpeed(dutyCycle);
  motorRF.run(FORWARD);
  motorLF.setSpeed(dutyCycle);
  motorLF.run(FORWARD);
  }while(abs(error)>=deadband);
  motorRB.run(RELEASE);
  motorLB.run(RELEASE);
  motorRF.run(RELEASE);
  motorLF.run(RELEASE);
  odometry();
  botDist = 0;
  pulseR = 0;
  }
  
void turnRight(){
  Reference = 26; //To be decided while calibration; For making turns, this will be a constant value and not determined by the sensor legic. We use the same variable, "Reference" to pass it to the PID function
  motorLB.setSpeed(startDuty2); 
  motorLB.run(FORWARD);
  motorRF.setSpeed(startDuty2);
  motorRF.run(BACKWARD);
  motorLF.setSpeed(startDuty2);
  motorLF.run(FORWARD);
  motorRB.setSpeed(startDuty2); //run motor at dutycycle defined by PID
  motorRB.run(BACKWARD);
  delay(50);
  do
  {
  updatedist();
  PID();
  motorLB.setSpeed(1.5*dutyCycle); //run motor at dutycycle defined by PID
  motorLB.run(FORWARD);
  motorRF.setSpeed(1.5*dutyCycle); 
  motorRF.run(BACKWARD);
  motorLF.setSpeed(1.5*dutyCycle);
  motorLF.run(FORWARD);
  motorRB.setSpeed(1.5*dutyCycle);
  motorRB.run(BACKWARD);
  }while(abs(error)>=deadband);
  motorLB.run(RELEASE);
  motorRF.run(RELEASE);
  motorLF.run(RELEASE);
  motorRB.run(RELEASE);
  delay(4000);
  pulseR = 0;       //Reinitialise encoder counts at the end of the function
  botDist = 0;        //We are moving in incremental steps; Initialise Dist = 0 and drive the motors till it reaches Reference
  double dTheta = 1.57079632;
  Theta = fmodf((Theta+dTheta),6.28318530);
  CosNow = cos(Theta);
  SinNow = sin(Theta);
  diffBotDist = diffBotDist - Reference;
  odometry();
  turnRightCounter += 1;
  if (turnRightCounter ==4)
  {turnRightCounter =0;}
 }
 
void odometry(){
  diffBotPosX = diffBotDist*SinNow;
  diffBotPosY = diffBotDist*CosNow;
  botPosX += diffBotPosX;
  botPosY += diffBotPosY;
 }
//======================
//Xbee Comm
//======================
void transmit(){
char s[36];
String U1 = toString(sensRd.US1);
String U2 = toString(sensRd.US2); 
String U2 = toString(sensRd.US2); 
String U3 = toString(sensRd.US3);
String Orient = toString(turnRightCounter);
String targetBool = toString(sensRd.readColor);
String xPos = toString(botPosX);
String yPos = toString(botPosY);
String x = "X"+xPos+"Y"+yPos+"P"+U1+"Q"+U2+"R"+U3+"O"+Orient+"T"+targetBool+"END";
for (int i =0; i<x.length();i++)
{s[i] = x[i];}
XBee.write(s);

}
//======================
//Convert to String
//======================
String toString(int Value){
String str = String(Value);
return str;	
}
String toString(double Value){
String str = String(Value);
return str;	
}
// --------------------------------Addon Functions End ------------------------------------------------
