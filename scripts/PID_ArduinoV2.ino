// --------------------------------Headers/ Setup begin------------------------------------------------
  
  #include <AFMotor.h>                  //Include Motor Library
  #include <math.h>                       //Include Math library
  AF_DCMotor motorRB(1,MOTOR12_64KHZ);    //setup Right side rear motor
  AF_DCMotor motorLB(2,MOTOR12_64KHZ);    //setup Left side rear motor
  AF_DCMotor motorRF(4,MOTOR12_64KHZ);    //setup Right front motor
  AF_DCMotor motorLF(3,MOTOR12_64KHZ);    //setup Left front motor
  #define pin_r 21                  // Encoder Pins Define
  #define pin_l 20 
  
// --------------------------------Headers/ Setup begin------------------------------------------------

//=======================
//MotoControl Definitions
//=======================

float encConst = 2.552544;    //Encoder constant of Encoder mm
float botWidth = 130;         //width of  robot in mm
int   pulseR = 0;
float Dr;           //distace travelled by the right wheels in mm
float botDist = 0;        //total distance travelled by the robot in mm
float diffBotDist;     
float PosX = 0;           //X Coordinate
float dPosX;
float PosY = 0;         //Y Coordinate
float dPosY;
float Theta = 0;        //Orientation in degrees
float SinNow = 0;
float CosNow = 1;
float botPosReference;
float Reference = 500;
double dutycycle;

int global_pose = 0;        //Encoder Position Value
double kp = 1;            //Proportional Gain
double kd = 7;            //Derivative Gain
double ki = 100;          //Intergral Gain
int maxoutput = 100;         //Maximum duty cycle
double deadband = 50;        //Error Margin
double dt = 0.008;          //Encoder Update Rate
double error,angle_error;
double derivative, integral;
double errorlast = 0;
double velocity;
double pi = 3.14159265359;

void setup(){
  
Serial.begin(9600);
pinMode(pin_r,INPUT);
pinMode(pin_l,INPUT);
attachInterrupt(digitalPinToInterrupt(pin_r),doEncoderR,CHANGE);  // trigger event
  
}
int iter = 1;
void loop(){
  if (iter ==1)
  {moveForward();
  }
  else{Serial.println("Got It");}
  iter+=1;}

void PID()
{
  
  errorlast = error;
  error = Reference - botDist;


  if (abs(error) <= deadband)
  {
    dutycycle = 0;
    error = 0;
  }
  else {
    dutycycle = (kp*error) + (kd*derivative) + (ki*integral);
  }

  if (dutycycle >= maxoutput)
    dutycycle = maxoutput;
  else if (dutycycle <= -maxoutput)
    dutycycle = -maxoutput;
  else

    integral += error*dt;

  derivative = (error - errorlast) / dt;

}

void doEncoderR(){
pulseR = pulseR + 1;
botDist = encConst * pulseR;
error = Reference - botDist;
PID();
Serial.print("dutycycle:");
Serial.println(dutycycle);
Serial.print("error");
Serial.println(error);
Serial.print("botDist");
Serial.println(botDist);
Serial.print("Ref");
Serial.println(Reference);
if(abs(error)<= deadband)
{Serial.print("made it");
 motorRB.run(RELEASE);
 motorRF.run(RELEASE);
 motorLB.run(RELEASE);
 motorLF.run(RELEASE);
}
}

void moveForward(){
  motorLB.setSpeed(130); 
  motorLB.run(FORWARD);
  motorRF.setSpeed(130);
  motorRF.run(FORWARD);
  motorLF.setSpeed(130);
  motorLF.run(FORWARD);
  motorRB.setSpeed(130); //run motor at dutycycle defined by PID
  motorRB.run(FORWARD);
  delay(300);
   do
  { 
  motorLB.setSpeed(dutycycle); 
  motorLB.run(FORWARD);
  motorRF.setSpeed(dutycycle);
  motorRF.run(FORWARD);
  motorLF.setSpeed(dutycycle);
  motorLF.run(FORWARD);
  motorRB.setSpeed(dutycycle); //run motor at dutycycle defined by PID
  motorRB.run(FORWARD);
  }while(abs(error)>=deadband);
 motorRB.run(RELEASE);
 motorRF.run(RELEASE);
 motorLB.run(RELEASE);
 motorLF.run(RELEASE);
  }