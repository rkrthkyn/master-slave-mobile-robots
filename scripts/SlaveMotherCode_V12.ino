/*MEEN 667 Mechatronics - Course Project
Arvind Srinivasa
Rohith Karthikeyan
Taimoor Daud Khan
Edit: 12/02/2017 Friday*/

// --------------------------------Headers/ Setup begin------------------------------------------------
#include <AFMotor.h>                   		//Include Motor Library
#include <math.h>                       	//Include Math library
#include <SoftwareSerial.h>          		//Include SoftwareSerial Library
AF_DCMotor motorRB(1,MOTOR12_64KHZ);    	//setup Right side rear motor
AF_DCMotor motorLB(2,MOTOR12_64KHZ);    	//setup Left side rear motor
AF_DCMotor motorRF(4,MOTOR34_64KHZ);    	//setup Right front motor
AF_DCMotor motorLF(3,MOTOR34_64KHZ);    	//setup Left front motor
#define pin_r 2                 			// Encoder Pin Define
// --------------------------------Headers/ Setup End------------------------------------------------

// --------------------------------Global Var begin ------------------------------------------------

//======================
//Sensor Pin Definitions
//======================

const int trigPin1 = 10;
const int echoPin1 = 9;
const int buzzerPin = 13;
int US2=0; 
//=======================
//MotoControl Definitions
//=======================

double encConst = 0.1859882;   //Encoder constant of Encoder cm
int   pulseR = 0;
double botDist = 0;         //total distance travelled by the robot in mm
int dutyCycle; 
int startDuty = 100;          //Duty cycle to start the motors
int startDuty2 = 240;           //Duty cycle to start the motors
double kp = 1;                //Proportional Gain
double kd = 7;                //Derivative Gain
double ki = 100;        //Intergral Gain
int maxoutput = 70;     //Maximum duty cycle
double deadband = 10;         //Error Margin
double dt = 0.008;            //Encoder Update Rate
double error;
double derivative, integral;
double errorlast = 0;
double pi = 3.14159265359;
double Reference;
int botOrient = 0;          // 1,2,3,4 values tell me orientation of bot

//=======================
//Xbee Definitions
//=======================

SoftwareSerial XBee(19,18); 		// RX, TX

//=======================
//Slave-drive definitions
//=======================

struct goSlave{
	int posInput;
	int boolTarget;
	int boolTurn;
	
};
goSlave slaveMotion;

//======================
//Setup/Loop Functions
//======================

void setup() {
Serial.begin(9600); 							//standard baud rate
XBee.begin(9600);								// baud rate for Xbe

/*For  Encoder Modules */
pinMode(pin_r,INPUT);

/*For Sensor Comm Modules */
pinMode(trigPin1, OUTPUT); 
pinMode(echoPin1, INPUT);
pinMode(buzzerPin, OUTPUT); 

/*ISR Definition for Encoders*/
attachInterrupt(digitalPinToInterrupt(pin_r),doEncoderR,CHANGE);
}
void loop() {
int US2_0 = Ultrasound_1();
slaveMotion = splitString(); 					// Get distance for travel
driveForward(US2_0,slaveMotion.posInput);
delay(2000);
if(slaveMotion.boolTarget ==1)
{
	digitalWrite(buzzerPin,HIGH);
	delay(1000);
	digitalWrite(buzzerPin,LOW);
  }
delay(2000);
if(slaveMotion.boolTurn == 1)
{
turnRight();
}

}


// --------------------------------Addon Functions Begin ------------------------------------------------
/*Get Slave Position Information*/
goSlave splitString(){
String readString;
goSlave x;
Serial.println("reached splitstring");
while (XBee.available() ==0){}
while (XBee.available()!= 0){ 
char c = XBee.read();                         
                       //Stops Serial.read
   readString += c;  
   delay(1) ;
 
}    
 Serial.println(readString);             //Prints the String in the format M---C--O--E ; We have this for evaluation, can get rid of this later
 int ind1 = readString.indexOf('C');         
 int ind2 = readString.indexOf('O');
    
 String distance   = readString.substring(1,ind1);    //distance is a string of the characters between M & C
// Serial.println(distance);
 String color      = readString.substring(ind1+1,ind2);  //color is a string of the characters between C & O
 //Serial.println(color);
 String orient     = readString.substring(ind2+1,ind2+2);      //orient is a string of the characters between O & E
 //Serial.println(orient);
 
 x.posInput    = distance.toInt();          //Converts string to int
 x.boolTarget  = color.toInt();
 x.boolTurn    = orient.toInt();
 readString  = "";        
 
 return x;
 }
 
int Ultrasound_1() {
long duration;
int distance;
 
digitalWrite(trigPin1, LOW);
delayMicroseconds(2);

// Sets the trigPin on HIGH state for 10 micro seconds
digitalWrite(trigPin1, HIGH);
delayMicroseconds(10);
digitalWrite(trigPin1, LOW);

// Reads the echoPin, returns the sound wave travel time in microseconds
duration = pulseIn(echoPin1, HIGH);

// Calculating the distance
distance= duration*0.034/2;

// Returns distance
return (distance);
}
void driveForward(int US2_0, int pos ){
Serial.println("Reached driveForward");
do
{
  motorLB.setSpeed(70); 
  motorLB.run(FORWARD);
  motorRF.setSpeed(70);
  motorRF.run(FORWARD);
  motorLF.setSpeed(70);
  motorLF.run(FORWARD);
  motorRB.setSpeed(70);    
  motorRB.run(FORWARD); 
  US2 = Ultrasound_1();
  }while(abs(US2-US2_0)< pos);
 
 motorRB.run(RELEASE);
 motorRF.run(RELEASE);
 motorLB.run(RELEASE);
 motorLF.run(RELEASE);
 pulseR = 0;       //Reinitialise encoder counts at the end of the function
 botDist = 0; 
}
 void doEncoderR(){
pulseR = pulseR + 1;
//Serial.println(pulseR);
}
void updatedist(){
  botDist = encConst * pulseR;
  error = Reference - botDist;
}
void turnRight(){
//  Serial.println("Reached turnRight");
  Reference = 22; //To be decided while calibration; For making turns, this will be a constant value and not determined by the sensor legic. We use the same variable, "Reference" to pass it to the PID function
  motorRB.setSpeed(startDuty2);
  motorRB.run(BACKWARD);
  motorLB.setSpeed(startDuty2); 
  motorLB.run(FORWARD);
  motorRF.setSpeed(startDuty2);
  motorRF.run(BACKWARD);
  motorLF.setSpeed(startDuty2);
  motorLF.run(FORWARD);
  delay(50);
  do
  {
//  Serial.print("Error= ");
//  Serial.println(error);
//  Serial.print("dutyCycle= ");
//  Serial.println(dutyCycle);  
  updatedist();
  PID();
  motorRB.setSpeed(1.5*dutyCycle); //run motor at dutycycle defined by PID
  motorRB.run(BACKWARD);
  motorLB.setSpeed(1.5*dutyCycle); 
  motorLB.run(FORWARD);
  motorRF.setSpeed(1.5*dutyCycle);
  motorRF.run(BACKWARD);
  motorLF.setSpeed(1.5*dutyCycle);
  motorLF.run(FORWARD);
  }while(abs(error)>=deadband);
 motorRB.run(RELEASE);
 motorRF.run(RELEASE);
 motorLB.run(RELEASE);
 motorLF.run(RELEASE);
 pulseR = 0;       //Reinitialise encoder counts at the end of the function
 botDist = 0;        //We are moving in incremental steps; Initialise Dist = 0 and drive the motors till it reaches Reference
 }
void PID(){
  
  errorlast = error;
  error = Reference - botDist;


  if (abs(error) <= deadband)
  {
    dutyCycle = 0;
    error = 0;
  }
  else {
    dutyCycle = (kp*error) + (kd*derivative) + (ki*integral);
  }

  if (dutyCycle >= maxoutput)
    dutyCycle = maxoutput;
  else if (dutyCycle <= -maxoutput)
    dutyCycle = -maxoutput;
  else

    integral += error*dt;

  derivative = (error - errorlast) / dt;

}
