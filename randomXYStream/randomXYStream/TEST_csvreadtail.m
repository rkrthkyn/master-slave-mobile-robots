function tests = TEST_csvreadtail()
%Syntax:
%runtests('TEST_csvreadtail.m')

tests = functiontests(localfunctions);

end

%% Setup and teardown functions
function setupOnce(testCase)
%This setup function runs once at the beginning of the entire
%TEST_csvreadtail function.

%Add files under test to the path
cd('C:\Users\Mike\Documents\HTC\gitlab\SignalProcessingPipeline\')
addpath(genpath('utility'));

%Set up the testing parameters
testCase.TestData.numLinesTotal = 10;
testCase.TestData.numColsTotal = 10;
testCase.TestData.tol = 1e-15;
testCase.TestData.tolString = sprintf('%%.%df',abs(log10(testCase.TestData.tol)));
testCase.TestData.table = rand(testCase.TestData.numLinesTotal);
testCase.TestData.filenameTable = 'testfileNumeric.csv';
testCase.TestData.tableWithHeader = createRandomTableWithHeaders(testCase.TestData.numLinesTotal,...
    testCase.TestData.numColsTotal);
testCase.TestData.filenameTableWithHeader = 'testFileWithHeader.csv';

%Write a sample numeric CSV file to read from during testing
dlmwrite(testCase.TestData.filenameTable,...
    testCase.TestData.table,...
    'delimiter',',',...
    'precision',testCase.TestData.tolString);

%Write a sample CSV with numeric data but strings for headers to read from
%during testing
writetable(testCase.TestData.tableWithHeader,...
    testCase.TestData.filenameTableWithHeader);

end

function teardownOnce(testCase)
%This teardown function runs once at the end of all the tests. Use it to
%delete the CSV files that were created.
fprintf('Tearing down after tests...\n');

delete(testCase.TestData.filenameTable);
delete(testCase.TestData.filenameTableWithHeader);
end

%% Helper functions
function l = randletters(sz)
%Generate a char array of the specified size, containing only lowercase
%letters

a = 97;
z = 122;
charIndices = randi([a,z],sz);
l = char(charIndices);
end


function T = createRandomTableWithHeaders(numRows,numCols)

cols = rand(numRows,numCols);
varNames = cell(1,numCols);

for iCol = 1:numCols
    %Generate a random sequence of 1-10 lowercase letters
    numLetters = randi([1,10]);
    str = randletters([1,numLetters]);
    
    varNames{iCol} = str;
end

T = array2table(cols,'VariableNames',varNames);

end

%% Test cases
function testreadfewerlines(testCase)
fprintf('Read fewer lines than are in the file...\t')
numLines = randi(testCase.TestData.numLinesTotal-1);

xActual = csvreadtail(testCase.TestData.filenameTable,...
    numLines);

assert(isequal(size(xActual), [numLines,testCase.TestData.numLinesTotal]),'Size is wrong')

d = abs(testCase.TestData.table(end-numLines+1:end,:) - xActual);
assert(all(all(d <= testCase.TestData.tol)),'Not equal')

fprintf('PASS\n')

end


function testreadnlines(testCase)
fprintf('Read exactly as many lines as there are in the file...\t');

xExpected = testCase.TestData.table;
filename = testCase.TestData.filenameTable;

xActual = csvreadtail(filename,testCase.TestData.numLinesTotal);

assert(isequal(size(xActual), [testCase.TestData.numLinesTotal, testCase.TestData.numColsTotal]),'Size is wrong')

d = abs(xExpected - xActual);
assert(all(all(d <= testCase.TestData.tol)),'Not equal')

fprintf('PASS\n')

end


function testreadmorelines(testCase)
fprintf('Attempt to read more lines than there are in the file...\t')

xExpected = testCase.TestData.table;
filename = testCase.TestData.filenameTable;
numLines = testCase.TestData.numLinesTotal + 1;
xActual = csvreadtail(filename,numLines);

assert(isequal(size(xActual),...
    [testCase.TestData.numLinesTotal,testCase.TestData.numColsTotal]),...
    'Size is wrong')

d = abs(xExpected - xActual);
assert(all(all(d <= testCase.TestData.tol)),'Not equal');

fprintf('PASS\n')

end


function testreadnonnumericcsv(testCase)
fprintf('Read non-numeric data...\t')

filename = testCase.TestData.filenameTableWithHeader;
s = fileread(filename);
xExpected = table2array(testCase.TestData.tableWithHeader);

%Read the entire csv file with csvreadtail
[numLines,~] = size(xExpected);

xActual = csvreadtail(filename,numLines);

assert(isequal(size(xActual), size(xExpected)),'Size is wrong')

%Ensure that all the numeric entries are equal
d = abs(xExpected(end-numLines+1:end,:) - xActual);
assert(all(all(d <= testCase.TestData.tol | isnan(xActual))),'Not equal')

fprintf('PASS\n')

end


function testreadcsvwithemptycells(testCase)
xExpected = testCase.TestData.table;

numToOmit = randi(min([testCase.TestData.numLinesTotal,testCase.TestData.numColsTotal]));

emptyRows = randperm(testCase.TestData.numLinesTotal,numToOmit);
emptyCols = randperm(testCase.TestData.numColsTotal,numToOmit);
numLines = randi([1,testCase.TestData.numLinesTotal]);
xExpected(emptyRows,emptyCols) = NaN;

filename = testCase.TestData.filenameTable;
tol = testCase.TestData.tol;
tolString = sprintf('%%.%df',abs(log10(tol)));
dlmwrite(filename,xExpected,'delimiter',',','precision',tolString);
s = fileread(filename);
s = strrep(s,'NaN','');
fid = fopen(filename,'w');
fprintf(fid,'%s',s);
fclose(fid);

xExpected = csvread(filename);
xActual = csvreadtail(filename,numLines);

assert(isequal(size(xExpected(end-numLines+1:end,:)), size(xActual)),'Size is wrong');
d = abs(xExpected(end-numLines+1:end,:) - xActual);
%Ensure all the non-blank entries are equal
assert(all(all(d <= testCase.TestData.tol | isnan(xActual))),'Not equal')

end


function speedtest(testCase)
%Try running the function many times and measuring the average execution
%time
numIterations = 50;
fprintf('Speed test - read 1 file %d times...\t',numIterations)
%Read the last 10% of the lines
numLines = ceil(0.1 * testCase.TestData.numLinesTotal);
filename = testCase.TestData.filenameTable;
xExpected = testCase.TestData.table;


t = zeros(1,numIterations);
tic
for i = 1:numIterations
    tic
    xActual = csvreadtail(filename,numLines);
    t(i) = toc;
    
    d = abs(xExpected(end-numLines+1,:) - xActual);
    assert(all(all(d <= testCase.TestData.tol)), 'Not equal');
end

fprintf('Mean time to read %d lines = %0.3f ms\n',numLines,mean(t)*1000)

end