% Rohith Karthikeyan - 10/30/2017
% Test 1: 11/15/2017 Wednesday
% Read String from Xbee

%-------------------------------------------------------
% Build map code
%-------------------------------------------------------

clear all;
close all;
clc;
instrreset;

botOrOldIP = 0;                          %Initialize botOrientOld to default start state

% Target Position Values Initialized

tPX = 0;                                     %Initialize xOld to default start state
tPY = 0;                                     %Initialize yOld to default start state

% bot/sensor values initialized


botOrOld = 1;
bPX0 = 0;
bPY0 = 0;
US10 = 0;
US20 = 0;

runCount = 0;  									% keep track of points2draw runs

%% Plot Object Graphic Attributes

masterPos = animatedline;                       % Initialize masterPos graphics object
masterPosUS = animatedline;                     % Initialize masterPos-US graphics object
mapContour = animatedline;                      % Initialize mapContour graphics object
mapContourUS = animatedline;                    % Initialize mapContour-US graphics object
mapTargets = animatedline;                      % Initialize mapTargets graphics object
setGraphicProperties(masterPos,mapContour,mapContourUS,masterPosUS,mapTargets);     % function initializes object graphic properties

%% Xbee Comm Initialize object
readVal = 'dummy'; % Initialize Dummy String
% Master ------------------------------------
xbeeTalkerMaster = serial('COM6', 'BaudRate', 9600,...
    'Terminator', 'CR', 'Parity', 'None'); % Specify port to read
fopen(xbeeTalkerMaster);
% Slave -------------------------------------
xbeeTalkerSlave = serial('COM5', 'BaudRate', 9600,...
    'Terminator', 'CR', 'Parity', 'None'); % Specify port to read
fopen(xbeeTalkerSlave);

%% Initialize Timer
a = tic;                                        % Control plot DAQ rate - initialize timer
    
%% Main Loop - All graphics happen in here
while(1<2)
colSens = 0;
b = toc(a);
if b>(1)
data = getString(xbeeTalkerMaster);
a =tic;
[bPX,bPY,set1X,set1Y,set2X, set2Y,set3X,set3Y,bPXU,bPYU,...
    set1XU,set1YU,set2XU, set2YU, set3XU,set3YU,bPX0,...
    bPY0,botOrOld,US10,US20,runCount,tPX,tPY,colSens] = points2Draw(...
    data,botOrOld,bPX0,bPY0,US10,US20,runCount,colSens,tPX,tPY,xbeeTalkerSlave);

% clearpoints(masterPos);
% addpoints(masterPos,bPX,bPY);
% addpoints(mapContour,set1X,set1Y);
% addpoints(mapContour,set2X,set2Y);
% addpoints(mapContour,set3X,set3Y);

addpoints(masterPosUS,bPXU,bPYU);
addpoints(mapContourUS,set1XU,set1YU);
addpoints(mapContourUS,set2XU,set2YU);
addpoints(mapContourUS,set3XU,set3YU);

drawnow;

if(colSens ==1)
addpoints(mapTargets,set1XU,set1YU);
drawnow;
end
end
end

%% Function Definitions Go Below Here
%=====================================================================

%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Sets Plot Object Graphic Properties
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function[] = setGraphicProperties(x,y,z,z1,z2)

x.LineStyle = 'none';         
y.LineStyle = 'none';    
z.LineStyle = 'none';
z1.LineStyle = 'none';
z2.LineStyle = 'none';

x.Color =   'red';
y.Color =   'red';
z.Color =   'green';
z1.Color =  'green';
z2.Color =  'magenta';

x.Marker =  '*';
y.Marker =  '.';
z.Marker =  'p';
z1.Marker = '*';
z2.Marker = 'd';

x.MarkerSize = 20;
y.MarkerSize = 20;
z.MarkerSize = 20;
z1.MarkerSize = 20;
z2.MarkerSize = 10;


z.MarkerFaceColor = 'green';
z.MarkerEdgeColor = 'black';

%axis([-20,50,0,50]);                              
end

%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Plot w/Conditional Select
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function [bPX,bPY,set1X,set1Y,set2X, set2Y,set3X,set3Y,bPXU,bPYU,...
    set1XU,set1YU,set2XU, set2YU, set3XU,set3YU,bPX0_out,...
    bPY0_out,botOrOldOP,US10_out,US20_out,runCountOP,tPXNew,tPYNew,colSens] = points2Draw(...
    data,botOrOldIP,bPX0_in,bPY0_in,US10_in,US20_in,runCountIP,colSens,tPXOld,tPYOld,xbeeTalkerSlave)

	% what are all these variables?
	% bP - bot position
	% U suffix - w/ultrasound based correction
	% tP - target position
	% OP/IP suffix - output or input arguments
	% set1/2/3 points to plot for each ultrasound sensor
	
botOrient = splitString(data,'O','T');
bPX = splitString(data,'X','Y');
bPY = splitString(data,'Y','P');
US1 = splitString(data,'P','Q');
US2 = splitString(data,'Q','R');
US3 = splitString(data,'R','O');
colSens  = splitString(data,'T','END');
boolTurn = 0;	% Reset the boolTurn variable;

% Here, we set the US1/2 start values to the first reading recorded by the sensors
% This should be done for the first run, to correct for initializing these vars as '0'

if (runCountIP ==0)
US10_in = US1;
US20_in = US2;
end

runCountOP = runCountIP+1;

switch botOrient

    case 0
       
	   % Plots without US correction
        set1X = bPX - US1;
        set1Y = bPY;
        set2X = bPX;
        set2Y = bPY +US2;
        set3X = bPX+US3;
        set3Y = bPY;        
        
        %Plots with US correction
        bPXU = bPX0_in-(US10_in-US1);
        bPYU = bPY0_in+(US20_in-US2);
        set1XU = bPXU - US1;
        set1YU = bPYU;
        set2XU = bPXU;
        set2YU = bPYU +US2;
        set3XU = bPXU+US3;
        set3YU = bPYU; 
                
		if(colSens == 1)
			%Update TP values
			tPXNew = bPXU;
			tPYNew = bPYU;
		else	
			tPXNew = tPXOld;
			tPYNew = tPYOld;
		end

		if(~isequal(botOrOldIP,botOrient))
			   %Update botTurn variables
			   boolTurn = 1;    
               bPX0_out = bPXU;
               bPY0_out = bPYU;
               US10_out = US1;
               US20_out = US2;
		else
				bPX0_out = bPX0_in;
				bPY0_out = bPY0_in;
				US10_out = US10_in;
				US20_out = US20_in;	
		end 

       
		if (colSens ==1 || ~isequal(botOrOldIP,botOrient))
			%Send String    
			moveDistance = abs(bPYU - tPYOld);
			sendThisString = horzcat('M',moveDistance,'C',colSens,'O',int2str(boolTurn),'E');
			disp(sendThisString);
            wrtString(xbeeTalkerSlave,sendThisString);
		end		
      
    
	case 1
		
		% Plots without US correction
        set1X = bPX;
        set1Y = bPY+US1;
        set2X = bPX+US2;
        set2Y = bPY;
        set3X = bPX;
        set3Y = bPY-US3;  
        
		%Plots with US correction
        bPXU = bPX0_in+(US20_in-US2);
        bPYU = bPY0_in+(US10_in-US1);
        set1XU = bPXU - US1;
        set1YU = bPYU;
        set2XU = bPXU;
        set2YU = bPYU +US2;
        set3XU = bPXU+US3;
        set3YU = bPYU; 

		if(colSens == 1)
			%Update TP values
			tPXNew = bPXU;
			tPYNew = bPYU;
		else	
			tPXNew = tPXOld;
			tPYNew = tPYOld;
		end

		if(~isequal(botOrOldIP,botOrient))
			   %Update botTurn variables
			   boolTurn = 1;    
               bPX0_out = bPXU;
               bPY0_out = bPYU;
               US10_out = US1;
               US20_out = US2;
		else
				bPX0_out = bPX0_in;
				bPY0_out = bPY0_in;
				US10_out = US10_in;
				US20_out = US20_in;	
		end 
	
				
		if (colSens ==1 || ~isequal(botOrOldIP,botOrient))
       	
            moveDistance = abs(bPXU - tPXOld);
            sendThisString = horzcat('M',moveDistance,'C',colSens,'O',int2str(boolTurn),'E');
            disp(sendThisString);
            wrtString(xbeeTalkerSlave,sendThisString);            

		end
        
    case 2
       
		 % Plots without US correction
        set1X = bPX;
        set1Y = bPY+US1;
        set2X = bPX+US2;
        set2Y = bPY;
        set3X = bPX;
        set3Y = bPY-US3;  
        
         %Plots with US correction
        bPXU = bPX0_in+(US10_in-US1);
        bPYU = bPY0_in-(US20_in-US2);
        set1XU = bPXU - US1;
        set1YU = bPYU;
        set2XU = bPXU;
        set2YU = bPYU +US2;
        set3XU = bPXU+US3;
        set3YU = bPYU;         
            
		if(colSens == 1)
			%Update TP values
			tPXNew = bPXU;
			tPYNew = bPYU;
		else	
			tPXNew = tPXOld;
			tPYNew = tPYOld;
		end

		if(~isequal(botOrOldIP,botOrient))
			   %Update botTurn variables
			   boolTurn = 1;    
               bPX0_out = bPXU;
               bPY0_out = bPYU;
               US10_out = US1;
               US20_out = US2;
		else
				bPX0_out = bPX0_in;
				bPY0_out = bPY0_in;
				US10_out = US10_in;
				US20_out = US20_in;	
		end		
				
		if (colSens ==1 || ~isequal(botOrOldIP,botOrient))
   
            moveDistance = abs(bPYU - tPYOld);
            sendThisString = horzcat('M',moveDistance,'C',colSens,'O',int2str(boolTurn),'E');
            disp(sendThisString);
            wrtString(xbeeTalkerSlave,sendThisString);	
	
		end

    case 3
    
		% Plots without US correction
		set1X = bPX;
        set1Y = bPY-US1;
        set2X = bPX -US2;
        set2Y = bPY;
        set3X = bPX;
        set3Y = bPY+US3; 
        
		%Plots with US correction
        bPXU = bPX0_in-(US20_in-US2);
        bPYU = bPY0_in-(US10_in-US1);
        set1XU = bPXU - US1;
        set1YU = bPYU;
        set2XU = bPXU;
        set2YU = bPYU +US2;
        set3XU = bPXU+US3;
        set3YU = bPYU;
		
		if(colSens == 1)
			%Update TP values
			tPXNew = bPXU;
			tPYNew = bPYU;
		else	
			tPXNew = tPXOld;
			tPYNew = tPYOld;
		end

		if(~isequal(botOrOldIP,botOrient))
			   %Update botTurn variables
			   boolTurn = 1;    
               bPX0_out = bPXU;
               bPY0_out = bPYU;
               US10_out = US1;
               US20_out = US2;
		else
				bPX0_out = bPX0_in;
				bPY0_out = bPY0_in;
				US10_out = US10_in;
				US20_out = US20_in;	
		end		
		

		if (colSens ==1 || ~isequal(botOrOldIP,botOrient))
		
        moveDistance = abs(bPXU - tPXOld);
        sendThisString = horzcat('M',moveDistance,'C',colSens,'O',int2str(boolTurn),'E');
        disp(sendThisString);
        wrtString(xbeeTalkerSlave,sendThisString);     	
		
		end
			
			
			
end
botOrOldOP = botOrient;
end

%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Function to get String from XbeeMaster
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function[inputString] = getString(s)
fprintf("I reached getString\n");
while(s.BytesAvailable==0)      % Check for available serial data
end
 inputString = fscanf(s);
 disp(inputString); %
end
 
%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Split string read from Xbee
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function[readVal] = splitString(inputString,pos1,pos2)
indexPos1 = strfind(inputString,pos1);
indexPos2 = strfind(inputString,pos2);
readVal = str2double(inputString(indexPos1+1:indexPos2-1));
end

%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Function to write to XbeeSlave
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function[] = wrtString(x,s)
fprintf("I reached wrtString\n");
fprintf(x,s);
end