% Rohith Karthikeyan - 10/30/2017
% Test 1: 11/15/2017 Wednesday
% Read String from Xbee

%-------------------------------------------------------
% Build map code
%-------------------------------------------------------

clear all;
close all;
clc;
instrreset;


% Target Position Values Initialized

tPX = 0;                                     %Initialize xOld to default start state
tPY = 0;                                     %Initialize yOld to default start state

% bot/sensor values initialized


botOrOld = 0;


runCount = 0;  									% keep track of points2draw runs

%% Plot Object Graphic Attributes

masterPos = animatedline;                       % Initialize masterPos graphics object
masterPosUS = animatedline;                     % Initialize masterPos-US graphics object
mapContour = animatedline;                      % Initialize mapContour graphics object
mapContourUS = animatedline;                    % Initialize mapContour-US graphics object
mapTargets = animatedline;                      % Initialize mapTargets graphics object
setGraphicProperties(masterPos,mapContour,mapTargets);     % function initializes object graphic properties

xlabel('Position - X (cm)');
ylabel('Position - Y (cm)');
title('Real-time plot of bot and map');
box on;

%% Xbee Comm Initialize object
readVal = 'dummy'; % Initialize Dummy String
% Master ------------------------------------
xbeeTalkerMaster = serial('COM10', 'BaudRate', 9600,...
    'Terminator', 'CR', 'Parity', 'None'); % Specify port to read
fopen(xbeeTalkerMaster);
% Slave -------------------------------------
xbeeTalkerSlave = serial('COM8', 'BaudRate', 9600,...
    'Terminator', 'CR', 'Parity', 'None'); % Specify port to read
fopen(xbeeTalkerSlave);

%% Initialize Timer
a = tic;                                        % Control plot DAQ rate - initialize timer
    
%% Main Loop - All graphics happen in here


while(1<2)
colSens = 0;
b = toc(a);
if b>(1)
data = getString(xbeeTalkerMaster);
a =tic;

[bPX,bPY,set1X,set1Y,set2X, set2Y,set3X,set3Y,...
    botOrOld,tPX,tPY,colSens] = points2Draw(...
    data,botOrOld,tPX,tPY,xbeeTalkerSlave);

clearpoints(masterPos);
addpoints(masterPos,bPX,bPY);
addpoints(mapContour,set1X,set1Y);
addpoints(mapContour,set2X,set2Y);
addpoints(mapContour,set3X,set3Y);


drawnow;
if(colSens ==1)
addpoints(mapTargets,tPX,tPY);
drawnow;
end
end
end

%% Function Definitions Go Below Here
%=====================================================================

%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Sets Plot Object Graphic Properties
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function[] = setGraphicProperties(x,y,z2)

x.LineStyle = 'none';         
y.LineStyle = 'none';    
z2.LineStyle = 'none';

x.Color =   'red';
y.Color =   'red';
z2.Color =  'magenta';

x.Marker =  'o';
y.Marker =  '.';
z2.Marker = 'd';

x.MarkerSize = 15;
y.MarkerSize = 20;
z2.MarkerSize = 15;

x.MarkerFaceColor = 'red';
z2.MarkerFaceColor = 'green';

x.MarkerEdgeColor = 'black';
z2.MarkerEdgeColor = 'black';

%axis([-20,50,0,50]);                              
end

%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Plot w/Conditional Select
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function [bPX,bPY,set1X,set1Y,set2X, set2Y,set3X,set3Y,...
  botOrOldOP,tPXNew,tPYNew,colSens] = points2Draw(...
    data,botOrOldIP, tPXOld,tPYOld,xbeeTalkerSlave)

	% what are all these variables?
	% bP - bot position
	% U suffix - w/ultrasound based correction
	% tP - target position
	% OP/IP suffix - output or input arguments
	% set1/2/3 points to plot for each ultrasound sensor
	
botOrient = splitString(data,'O','T');
bPX = splitString(data,'X','Y');
bPY = splitString(data,'Y','P');
US1 = splitString(data,'P','Q');
US2 = splitString(data,'Q','R');
US3 = splitString(data,'R','O');
colSens  = splitString(data,'T','END');
boolTurn = 0;	% Reset the boolTurn variable;

% Here, we set the US1/2 start values to the first reading recorded by the sensors
% This should be done for the first run, to correct for initializing these vars as '0'

switch botOrient

    case 0
       
	   % Plots without US correction
        set1X = bPX - US1;
        set1Y = bPY;
        set2X = bPX;
        set2Y = bPY +US2;
        set3X = bPX+US3;
        set3Y = bPY;        
                
		if(colSens == 1)
			%Update TP values
			tPXNew = bPX;
			tPYNew = bPY;
		else	
			tPXNew = tPXOld;
			tPYNew = tPYOld;
		end

		if(~isequal(botOrOldIP,botOrient))
			   %Update botTurn variable
			   boolTurn = 1;  
		end 

       
		if (colSens == 1 || ~isequal(botOrOldIP,botOrient))
			%Send String    
			moveDistance = abs(bPX - tPXOld);
			sendThisString = horzcat('M',int2str(moveDistance),'C',int2str(colSens),'O',int2str(boolTurn),'E');
			disp(sendThisString);
            wrtString(xbeeTalkerSlave,sendThisString);
		end		
      
    
	case 1
		
		% Plots without US correction
        set1X = bPX;
        set1Y = bPY+US1;
        set2X = bPX+US2;
        set2Y = bPY;
        set3X = bPX;
        set3Y = bPY-US3;  
 


		if(colSens == 1)
			%Update TP values
			tPXNew = bPX;
			tPYNew = bPY;
		else	
			tPXNew = tPXOld;
			tPYNew = tPYOld;
		end

		if(~isequal(botOrOldIP,botOrient))
			   %Update botTurn variables
			   boolTurn = 1;   
		end 
	
				
		if (colSens ==1 || ~isequal(botOrOldIP,botOrient))
       	    moveDistance = abs(bPY - tPYOld);
           
            sendThisString = horzcat('M',int2str(moveDistance),'C',int2str(colSens),'O',int2str(boolTurn),'E');
            disp(sendThisString);
            wrtString(xbeeTalkerSlave,sendThisString);            

		end
        
    case 2
       
		 % Plots without US correction
        set1X = bPX;
        set1Y = bPY+US1;
        set2X = bPX+US2;
        set2Y = bPY;
        set3X = bPX;
        set3Y = bPY-US3;          
            
            if(colSens == 1)
			%Update TP values
			tPXNew = bPX;
			tPYNew = bPY;
            else	
			tPXNew = tPXOld;
			tPYNew = tPYOld;
            end

            if(~isequal(botOrOldIP,botOrient))
			   %Update botTurn variables
			   boolTurn = 1;    
            end		
				
		if (colSens ==1 || ~isequal(botOrOldIP,botOrient))
            moveDistance = abs(bPX - tPXOld);
           
            sendThisString = horzcat('M',int2str(moveDistance),'C',int2str(colSens),'O',int2str(boolTurn),'E');
            disp(sendThisString);
            wrtString(xbeeTalkerSlave,sendThisString);	
	
		end

    case 3
    
		% Plots without US correction
		set1X = bPX;
        set1Y = bPY-US1;
        set2X = bPX -US2;
        set2Y = bPY;
        set3X = bPX;
        set3Y = bPY+US3; 
		
		if(colSens == 1)
			%Update TP values
			tPXNew = bPX;
			tPYNew = bPY;
		else	
			tPXNew = tPXOld;
			tPYNew = tPYOld;
		end

		if(~isequal(botOrOldIP,botOrient))
			   %Update botTurn variables
			   boolTurn = 1;    
		end		
		

		if (colSens ==1 || ~isequal(botOrOldIP,botOrient))
		moveDistance = abs(bPY - tPYOld);
        sendThisString = horzcat('M',int2str(moveDistance),'C',int2str(colSens),'O',int2str(boolTurn),'E');
        disp(sendThisString);
        wrtString(xbeeTalkerSlave,sendThisString);     	
		
		end
			
			
			
end
botOrOldOP = botOrient;
end

%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Function to get String from XbeeMaster
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function[inputString] = getString(s)
fprintf('I reached getString\n');
while(s.BytesAvailable==0)      % Check for available serial data
end
 inputString = fscanf(s);
 disp(inputString); %
end
 
%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Split string read from Xbee
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function[readVal] = splitString(inputString,pos1,pos2)
indexPos1 = strfind(inputString,pos1);
indexPos2 = strfind(inputString,pos2);
readVal = str2double(inputString(indexPos1+1:indexPos2-1));
end

%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Function to write to XbeeSlave
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function[] = wrtString(x,s)
fprintf('I reached wrtString\n');
fprintf(x,s);
end